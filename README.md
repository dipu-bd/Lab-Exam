# Lab-Exam

### Basic Idea  

There will be two different application. One for server and another one for the clients. In the server application, passwords for all participants will be auto-generated and printed. Clients can login using their registration number and passwords and IP address of the server.    

After the exam begins question description will be shown to the participants. They have to write and submit their codes from inside of the application. There should not any way to copy-paste code or view it by another method. And one cannot go outside the application until the exam is finished.   

Server application will listen to all clients and download and store submitted solutions into separate folders in the server PC.   


### Developer
[Sudipto Chandra Dipu](https://github.com/dipu-bd)     
<dipu.sudipta@gmail.com>     